package ru.knuckles.smsdroid;

/**
 * Copyright (c) 2013, The smsdroid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.telephony.SmsMessage;
import android.util.Log;

public class SmsReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras == null || !intent.getAction().equals(SMS_RECEIVED_ACTION))
            return;

        Object[] messagePDUs = (Object[]) extras.get(SMS_EXTRA_NAME);
        if (messagePDUs.length != 1) {
            Log.w(getClass().getSimpleName(), String.format(
                "Whoa nigger! I've got %d PDUs. I quit this!",
                messagePDUs.length));
            return;
        }

        SmsMessage message = SmsMessage.createFromPdu((byte[]) messagePDUs[0]);
        if (isWhitelisted(context, message) || !detectSpam(message))
            return;

        Log.d(getClass().getSimpleName(),
            String.format(
                "Spam detected! Sender: '%s'; body: '%s'",
                message.getOriginatingAddress(),
                message.getMessageBody()));
        int id = (int) Spambox.getInstance(context).putMessage(message);
        abortBroadcast();
        notifySpamBlocked(context, intent, message, id);
    }

    private void notifySpamBlocked(Context context, Intent intent, SmsMessage message, int messageId) {
        NotificationManager notificationManager =
            (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification spamNotification = new Notification();
        spamNotification.setLatestEventInfo(
            context.getApplicationContext(),
            context.getString(R.string.app_name),
            message.getOriginatingAddress(),
            PendingIntent.getActivity(context, 1, new Intent(context, MessagesActivity.class), 0) );
        spamNotification.icon = R.drawable.ic_notify;
        spamNotification.tickerText = context.getString(R.string.notifictaion_text, message.getOriginatingAddress());
        notificationManager.notify(messageId, spamNotification);
    }

    private boolean isWhitelisted(Context context, SmsMessage message) {
        return WhiteList.getInstance(context).hasRecord(message.getOriginatingAddress());
    }

    private boolean detectSpam(SmsMessage message) {
        String senderAddress = message.getOriginatingAddress();
        int messageTOA = PhoneNumberUtils.toaFromString(senderAddress);
        // Simply mark spam everything comming from "strange", non-real-subscriber numbers.
        return
            messageTOA != PhoneNumberUtils.TOA_International ||
            !PhoneNumberUtils.isGlobalPhoneNumber(senderAddress) ||
            !PhoneNumberUtils.isWellFormedSmsAddress(senderAddress);
    }

    private static final String SMS_RECEIVED_ACTION = "android.provider.Telephony.SMS_RECEIVED";
    private static final String SMS_EXTRA_NAME = "pdus";
}
