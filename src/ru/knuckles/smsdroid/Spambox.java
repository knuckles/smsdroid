package ru.knuckles.smsdroid;

/**
 * Copyright (c) 2013, The smsdroid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.telephony.SmsMessage;

import java.util.*;

public class Spambox {
    public static interface Observer {
        public void onMessageAdded(long messageID);
    }

    public static Spambox getInstance(Context context) {
        if (_instance == null)
            _instance = new Spambox(context);
        return _instance;
    }

    public long putMessage(SmsMessage message) {
        SQLiteDatabase db = _dbHelper.getReadableDatabase();
        ContentValues messageValues = new ContentValues();
        messageValues.put(DatabaseHelper.COL_SENDER, message.getOriginatingAddress());
        messageValues.put(DatabaseHelper.COL_BODY, message.getMessageBody());
        long messageID = db.insertOrThrow(
            DatabaseHelper.TABLE_SPAM,
            null,
            messageValues);
        notifyObservers(messageID);
        return messageID;
    }

    public Message getMessage(long rowID) {
        Cursor cursor = getMessagesCursor(DatabaseHelper.COL_ID + " = " + rowID);
        try {
            if (cursor.getCount() < 1)
                throw new IllegalArgumentException();
            cursor.moveToFirst();
            Message result = new Message(
                cursor.getLong(cursor.getColumnIndex(DatabaseHelper.COL_ID)),
                cursor.getString(cursor.getColumnIndex(DatabaseHelper.COL_SENDER)),
                cursor.getString(cursor.getColumnIndex(DatabaseHelper.COL_BODY))
            );
            assert result.getID() == rowID;
            return result;
        }
        finally {
            cursor.close();
        }
    }

    public Cursor getMessagesCursor(String selection) {
        SQLiteDatabase db = _dbHelper.getReadableDatabase();
        return db.query(
            DatabaseHelper.TABLE_SPAM,
            null,       // columns
            selection,  // selection
            null,       // selectionArgs
            null,       // groupBy
            null,       // having
            null        // orderBy
        );
    }

    public void addObserver(Observer observer) {
        _observers.add(observer);
    }

    public void removeObserver(Observer observer) {
        _observers.remove(observer);
    }

    // PRIVATE

    private Spambox(Context context) {
        _dbHelper = new DatabaseHelper(context);
        _observers = new HashSet<Observer>();
    }

    private void notifyObservers(long messageID) {
        for (Observer observer : _observers) {
            try {
                observer.onMessageAdded(messageID);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static Spambox _instance;
    private DatabaseHelper _dbHelper;
    private Set<Observer> _observers;
}
