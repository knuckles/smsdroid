package ru.knuckles.smsdroid;

/**
 * Copyright (c) 2013, The smsdroid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class WhiteList {
    public static WhiteList getInstance(Context context) {
        if (_instance == null)
            _instance = new WhiteList(context);
        return _instance;
    }

    private WhiteList(Context context) {
        _dbHelper = new DatabaseHelper(context);
    }

    private static WhiteList _instance;

    private final DatabaseHelper _dbHelper;

    public boolean hasRecord(String address) {
        assert address != null;

        Cursor cursor = getSendersCursor(
            String.format("%s = '%s'", DatabaseHelper.COL_SENDER, address),
            1);
        try {
            return cursor.getCount() > 0;
        }
        finally {
            cursor.close();
        }
    }

    public String getSender(long senderID) {
        Cursor cursor = getSendersCursor(
            DatabaseHelper.COL_ID + " = " + senderID,
            1);
        try {
            if (cursor.getCount() < 1)
                throw new IllegalArgumentException();
            cursor.moveToFirst();
            return cursor.getString(cursor.getColumnIndex(DatabaseHelper.COL_SENDER));
        }
        finally {
            cursor.close();
        }
    }

    public Cursor getSendersCursor(String selection, int limit) {
        SQLiteDatabase db = _dbHelper.getReadableDatabase();
        return db.query(
            DatabaseHelper.TABLE_WHITELIST,
            null,       // columns
            selection,  // selection
            null,       // selectionArgs
            null,       // groupBy
            null,       // having
            null,       // orderBy
            limit > 0 ? String.valueOf(limit) : null
        );
    }

    public long addRecord(String address) {
        assert address != null;
        assert !address.isEmpty();

        SQLiteDatabase db = _dbHelper.getReadableDatabase();
        ContentValues whiteNumberVals = new ContentValues();
        whiteNumberVals.put(DatabaseHelper.COL_SENDER, address);
        return db.insertOrThrow(
            DatabaseHelper.TABLE_WHITELIST,
            null,
            whiteNumberVals
        );
    }

    public void removeRecord(long senderID) {
        SQLiteDatabase db = _dbHelper.getReadableDatabase();
        int removed = db.delete(DatabaseHelper.TABLE_WHITELIST, DatabaseHelper.COL_ID + " = " + senderID, null);
        if (removed < 1)
            throw new IllegalArgumentException();
    }
}
