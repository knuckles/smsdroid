package ru.knuckles.smsdroid;

/**
 * Copyright (c) 2013, The smsdroid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

import android.app.*;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

public class MessagesActivity extends Activity implements Spambox.Observer {
    // Activity

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_screen);

        _spamListView = (ListView) findViewById(R.id.spamList);
        _whiteListView = (ListView) findViewById(R.id.whiteList);

        _whiteList = WhiteList.getInstance(this);
        _spamBox = Spambox.getInstance(this);
        _spamBox.addObserver(this);

        setupTabs();
        setupSpamList();
        setupWhiteList();
    }

    @Override
    public void onStart() {
        super.onStart();
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    // Spambox.Observer

    @Override
    public void onMessageAdded(long messageID) {
        _spamCursor.requery();
    }

    // PRIVATE

    private void setupTabs() {
        TabHost tabs = (TabHost) findViewById(R.id.tabHost);
        tabs.setup();
        TabHost.TabSpec spamBoxTabSpec = tabs.newTabSpec("spamBox");
        spamBoxTabSpec.setContent(R.id.spamBoxTab);
        spamBoxTabSpec.setIndicator(getString(R.string.spamBox));
        tabs.addTab(spamBoxTabSpec);
        TabHost.TabSpec whiteListTabSpec = tabs.newTabSpec("whiteList");
        whiteListTabSpec.setContent(R.id.whiteListTab);
        whiteListTabSpec.setIndicator(getString(R.string.whiteList));
        tabs.addTab(whiteListTabSpec);
        tabs.setCurrentTab(0);
    }

    private void setupSpamList() {
        _spamCursor = _spamBox.getMessagesCursor(null);
        startManagingCursor(_spamCursor);
        ListAdapter _spamboxAdapter = new SimpleCursorAdapter(
            this,
            android.R.layout.two_line_list_item,
            _spamCursor,
            new String[]{
                DatabaseHelper.COL_SENDER,
                DatabaseHelper.COL_BODY
            },
            new int[]{
                android.R.id.text1,
                android.R.id.text2
            }
        );
        _spamListView.setAdapter(_spamboxAdapter);
        _spamListView.setOnItemClickListener(this._messageClickListener);
    }

    private void setupWhiteList() {
        _sendersCursor = _whiteList.getSendersCursor(null, 0);
        startManagingCursor(_sendersCursor);
        ListAdapter whiteListAdapter = new SimpleCursorAdapter(
            this,
            android.R.layout.activity_list_item,
            _sendersCursor,
            new String[]{
                DatabaseHelper.COL_SENDER
            },
            new int[]{
                android.R.id.text1
            }
        );
        _whiteListView.setAdapter(whiteListAdapter);
        _whiteListView.setOnItemClickListener(this._senderClickListener);
    }

    private AdapterView.OnItemClickListener _messageClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int itemPos, long messageID) {
            showMessageDialog(messageID);
        }
    };

    private AdapterView.OnItemClickListener _senderClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int itemPos, long senderID) {
            showSenderDialog(senderID);
        }
    };

    private void showMessageDialog(final long messageID) {
        final Message message = _spamBox.getMessage(messageID);
        if (message == null)
            return;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(message.getSender());
        builder.setPositiveButton(R.string.addToWhiteList, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                addToWhiteList(message.getSender());
            }
        });
        builder.create().show();
    }

    private void showSenderDialog(final long senderID) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(_whiteList.getSender(senderID));
        builder.setPositiveButton(R.string.removeFromWhiteList, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                removeFromWhiteList(senderID);
            }
        });
        builder.create().show();
    }

    private void addToWhiteList(String sender) {
        if (!_whiteList.hasRecord(sender)) {
            _whiteList.addRecord(sender);
            _sendersCursor.requery();
        }
        else
            Toast.makeText(this, getString(R.string.already_whitelisted), 5).show();
    }

    private void removeFromWhiteList(long senderID) {
        _whiteList.removeRecord(senderID);
        _sendersCursor.requery();
    }

    Cursor _spamCursor;
    private Spambox _spamBox;
    Cursor _sendersCursor;
    private WhiteList _whiteList;
    private ListView _spamListView;
    private ListView _whiteListView;
}
