package ru.knuckles.smsdroid;

/**
 * Copyright (c) 2013, The smsdroid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

public class Message {
    public Message(long id, String sender, String body) {
        _id = id;
        _sender = sender;
        _body = body;
    }

    public long getID() {
        return _id;
    }

    public String getBody() {
        return _body;
    }

    public String getSender() {
        return _sender;
    }

    private final long _id;
    private final String _sender;
    private final String _body;
}
