package ru.knuckles.smsdroid;

/**
 * Copyright (c) 2013, The smsdroid authors.
 * All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        createSpamBox(database);
        createWhiteList(database);
    }

    private void createSpamBox(SQLiteDatabase database) {
        database.execSQL(
            "CREATE TABLE " + TABLE_SPAM + " ( " +
                COL_ID      + " INTEGER PRIMARY KEY, " +
                COL_SENDER  + " CHARACTER, " +
                COL_BODY    + " CHARACTER " +
            ");"
        );
        database.execSQL(
            String.format(
                "CREATE INDEX %s ON %s (%s);",
                TABLE_SPAM + "_" + COL_SENDER,  // WARN: This was just COL_SENDER in v1 schema!
                TABLE_SPAM,
                COL_SENDER));
    }

    private void createWhiteList(SQLiteDatabase database) {
        database.execSQL(
            "CREATE TABLE " + TABLE_WHITELIST + " ( " +
                COL_ID + " INTEGER PRIMARY KEY, " +
                COL_SENDER + " CHARACTER " +
                ");"
        );
        database.execSQL(
            String.format(
                "CREATE UNIQUE INDEX %s ON %s (%s);",
                TABLE_WHITELIST + "_" + COL_SENDER,
                TABLE_WHITELIST,
                COL_SENDER));
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int fromVersion, int toVersion) {
        switch (toVersion) {
            case 2:
                createWhiteList(database);
            case 3:
                break;
        }
    }

    public static final String TABLE_SPAM = "spam";
    public static final String TABLE_WHITELIST = "whitelist";
    public static final String COL_ID = "_id";
    public static final String COL_SENDER = "sender";
    public static final String COL_BODY = "body";

    private static final String DB_NAME = "data.sqlite";
    private static final int VERSION = 2;
}
